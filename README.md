## Starter

Simple and light theme for Hugo static site generator.

## CSS

To generate *_syntax.css* file:

~~~sh
hugo gen chromastyles --style=trac > scss/_syntax.css
~~~

To see all available styles: https://help.farbox.com/pygments.html


Note: It would be required to remove black background color for .chroma from *_syntax.css* file.

To generate style.css file:

~~~sh
sassc  --style compressed scss/style.scss static/css/_syntax.css
~~~
